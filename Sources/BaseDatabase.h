#ifndef BaseDatabase_H
#define BaseDatabase_H

#include <iostream>
#include <string>
#include <vector>
#include "../Sqlite3/sqlite3.h"

//#define NOTFOUND = -1;

using std::string;


    enum DbErrors {
        notfound = -1
    };
    struct BaseEntry {
//        virtual ~BaseEntry() = 0;
    };

class BaseDatabase{
    public:
        BaseDatabase(std::string dbFilename);
        ~BaseDatabase();

        int StartTransaction();
        int CommitTransaction();

    protected:
        std::string _dbFilename = "";
        sqlite3* _DB;
        
        bool TransactionInProgress = false;


        virtual sqlite3* OpenDatabase(string dbFilename);
        virtual int InitDatabase(sqlite3 * dB) = 0;

        virtual int ImportEntry(BaseEntry & entryToImport) = 0;
        

        int getLastRowId();
        int getLastRowIdFromTable(string tableName);
        std::vector<string> GetFirstRowOfSelect(string selectSql);
        std::vector<std::vector<string>> GetAllRowsOfSelect(string selectSql);
        int ExecuteSql(string statment);
};

#endif