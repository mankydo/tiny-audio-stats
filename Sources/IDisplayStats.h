#ifndef IDisplayStats_H
#define IDisplayStats_H

#include "IAppController.h"
#include "AppController.h"
#include "MusicDbDatatypes.h"

class IDisplayStats
{
protected:

    IAppController * _appController;

public:
    virtual void Init(IAppController * appControllerToUse) = 0;

    virtual ~IDisplayStats() {
        _appController->DeleteDisplayWorker(this);
        printf("Worker is dying!\n");
    };

    musicDb::DayDate dateFrom = musicDb::DayDate(0,0,0);
    musicDb::DayDate dateTo = musicDb::DayDate(0,0,0);

    virtual void DisplayImportStatus(musicDb::ImportedEntry parsedEntry, int currentImportPosition, int TotalImportCount) = 0;
    virtual void ImportDatabaseComplete(int timeToComplete) = 0;
    virtual void DisplayTopAlbums(std::vector<std::pair<musicDb::album, int>> listOfAlbumsToDisplay) = 0;
    virtual void DisplayTopTracks(std::vector<std::pair<musicDb::track, int>> listOfTracksToDisplay) = 0;
    virtual void DisplayTopArtists(std::vector<std::pair<musicDb::artist, int>> listOfArtistToDisplay) = 0;
    virtual void DisplayInfo(std::string info) = 0;
    virtual void DisplayOverallStats(std::vector<std::string>) = 0;
};

#endif