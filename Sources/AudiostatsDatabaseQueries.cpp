#include "AudiostatsDatabase.h"

std::vector<std::pair<musicDb::track, int>> musicDb::AudiostatsDatabase::TopTracksInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned)
{
    std::vector<std::pair<track, int>> result; 
    int resulterrors = DbErrors::notfound;

    std::vector<std::vector<string>> resultSql = GetAllRowsOfSelect(
        "SELECT count(t.id) _count, t.name track, an.name artist, al.name album"
        " from Tracks t, TracksListened tl, ArtistsNames an, Albums al"
        " WHERE tl.trackId = t.id and an.artistId = t.artistId and t.albumId = al.id"
        " and (tl.UnixTime BETWEEN "+ std::to_string(timeFrom) + " and "+ std::to_string(timeTo) +")"
        " GROUP by t.id"
        " UNION"
        " SELECT count(t.id) _count, t.name track, an.name artist, \"\""
        " from Tracks t, TracksListened tl, ArtistsNames an"
        " WHERE tl.trackId = t.id and an.artistId = t.artistId and t.albumId is null"
        " and (tl.UnixTime BETWEEN "+ std::to_string(timeFrom) + " and "+ std::to_string(timeTo) +")"
        " GROUP by t.id"
        " ORDER by _count DESC;"
        );

    int count = 1;
    if(resultSql.size() > 0)
    {
        for(std::vector<string> resultLine : resultSql)
        {
            track entry;
            entry.name = resultLine[1];
            entry.artistName = resultLine[2];
            entry.albumName = resultLine[3];

            result.push_back(std::make_pair(entry,stoi(resultLine[0])));
            count++;
            if(count > numberOfEntriesReturned)
                break;
        }
    }

    printf("\nTop tracks counted, yay!\n");

    return result;

}
std::vector<std::pair<musicDb::artist, int>> musicDb::AudiostatsDatabase::TopArtistsInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned)
{
    std::vector<std::pair<artist, int>> result;
    int resulterrors = DbErrors::notfound;

    std::vector<std::vector<string>> resultSql = GetAllRowsOfSelect(
            "SELECT count(an.artistId) artist_count, an.artistId, an.name artist"
            " from Tracks t, ArtistsNames an, TracksListened tl "
            " WHERE t.artistId = an.artistId and tl.trackId = t.id "
            " and (tl.UnixTime BETWEEN "+ std::to_string(timeFrom) + " and "+ std::to_string(timeTo) +")"
            " GROUP by an.artistId ORDER by artist_count DESC;"
        );

    int count = 1;
    if(resultSql.size() > 0)
    {
        for(std::vector<string> resultLine : resultSql)
        {
            artist entry;
            entry.id = stoi(resultLine[1]);
            entry.name = resultLine[2];

            result.push_back(std::make_pair(entry,stoi(resultLine[0])));
            count++;
            if(count > numberOfEntriesReturned)
                break;
        }
    }

    printf("\nTop artists counted, yay!\n");

    return result;
}
std::vector<std::pair<musicDb::album, int>>  musicDb::AudiostatsDatabase::TopAlbumsInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned)
{
    std::vector<std::pair<album, int>> result;
    int resulterrors = DbErrors::notfound;

    std::vector<std::vector<string>> resultSql = GetAllRowsOfSelect(
        "SELECT count(*) cnt, al.id, al.name, al.artistId, al.artistName from TracksListened tl, Tracks t,"
        " (	SELECT count(*) artistsOnAlbum, a.id id, a.name name, "
        "    case when count(*) > 1 then \"<Various>\" else an.name end artistName,"
        "    case when count(*) > 1 then -1 else an.artistId end artistId"
        "    from Albums a, "
        "    (select an.artistId, name, albumId from ArtistsNames an, AlbumArtists aa where an.artistId = aa.artistId) an"
        "    where an.albumId = a.id"
        "    group by a.id) al"
        " where tl.trackId = t.id"
        " and t.albumId = al.id"
        " and (tl.UnixTime BETWEEN "+ std::to_string(timeFrom) + " and "+ std::to_string(timeTo) +")"
        " group by al.id order by cnt desc"
        );

    int count = 1;
    if(resultSql.size() > 0)
    {
        for(std::vector<string> resultLine : resultSql)
        {
            album entry;
            entry.id = stoi(resultLine[1]);
            entry.name = resultLine[2];
            entry.artistId = stoi(resultLine[3]);
            entry.artistName = resultLine[4];

            result.push_back(std::make_pair(entry,stoi(resultLine[0])));
            count++;
            if(count > numberOfEntriesReturned)
                break;
        }
    }

    printf("\nTop albums counted, yay!\n");

    return result;
}
