#ifndef GTKMM_PopupDatePickGtk_H
#define GTKMM_PopupDatePickGtk_H

#include <gtkmm.h>
#include "MusicDbDatatypes.h"

class PopupDatePickGtk
{
    private:
        Gtk::Window * _popupWindowToWorkWith;
        Gtk::Button * _buttonToWorkWith;
        Gtk::Calendar * _calendarToWorkWith;
        musicDb::DayDate * _dateToReturnTo;

        sigc::connection _signalConnect_FocusOut;
        sigc::connection _signalConnect_DayChosen;
        sigc::connection _signalConnect_MonthChosen;

        // A workaround for checking if window was opened recently to exclude 
        //   false reactions to Focus Out event that happens right after showing/opening a window
        time_t _popupOpenTime;
        // Timeout to wait before Focus Out event signal can start to work
        const int _openedWindowTimeout = 1;

        //another workaround because changing month triggering a day selection event/signal to happen
        time_t _monthChangedAt = 0;

    public:
//        PopupDatePickGtk(musicDb::DayDate * dateToReturnTo, Gtk::Window * popupWindow, Gtk::Button * buttonThatCalled);
        void ShowPopup(musicDb::DayDate * dateToReturnTo, Gtk::Window * popupWindow, Gtk::Button * buttonThatCalled);
        void ClosePopup(bool returnResults);
//        ~PopupDatePickGtk();
};

#endif /* GTKMM_PopupDatePickGtk_H */
