#ifndef AppController_H
#define AppController_H

#include "IAppController.h"

class AppController : public IAppController
{
private:

    musicDb::AudiostatsDatabase * _innerAudioDB = nullptr;

public:
    AppController(musicDb::AudiostatsDatabase * audioStatsDb)
    {
        Init(audioStatsDb);
    };

    AppController(std::string audioStatsDbFilename)
    {
        Init(audioStatsDbFilename);
    };

    void Init(musicDb::AudiostatsDatabase * audioStatsDb) override{
        if(_innerAudioDB != nullptr) {
            delete(_innerAudioDB);
            _innerAudioDB = nullptr;
        }
        _audioDB = audioStatsDb;
    };
    
    void Init(std::string audioStatsDbFilename) override{

        musicDb::AudiostatsDatabase * audioDatabase = new musicDb::AudiostatsDatabase(audioStatsDbFilename);
        if(_innerAudioDB != nullptr) {
            delete(_innerAudioDB);
        }
        _innerAudioDB = audioDatabase;
        _audioDB = audioDatabase;
    };

    void AddDisplayWorker(IDisplayStats * workerToAdd) override
    {
        _displayWorkers.push_back(workerToAdd);
    }
    void DeleteDisplayWorker(IDisplayStats * workerToDelete) override
    {
        printf("Deleting workers; count %i\n", (int)_displayWorkers.size());
        for(auto it = _displayWorkers.begin(); it != _displayWorkers.end();){
            if(*it == workerToDelete){
                it = _displayWorkers.erase(it);
            } else {
                it++;
            }
        }
        printf("Done deleting workers; count %i\n", (int)_displayWorkers.size());
    }

    virtual void ImportDatabase(string csvFilenameToImport, int timeToTruncNewEntries) override;
    virtual void ImportDatabaseBackground(string csvFilenameToImport, int timeToTruncNewEntries) override;
    virtual bool GetImportStatus(musicDb::ImportedEntry & parsedEntry, int & currentImportPosition, int & TotalImportCount) override;
    virtual void ImportDatabaseComplete(int timeToComplete) override;

    virtual void DisplayTopAlbums(time_t timeFrom, time_t timeTo) override;
    virtual void DisplayTopTracks(time_t timeFrom, time_t timeTo) override;
    virtual void DisplayTopArtists(time_t timeFrom, time_t timeTo) override;

    virtual void DisplayStatsAll() override;
    virtual void DisplayStats(TimeIntervalToDisplay intervalToUse) override;
    virtual void DisplayInfo(std::string info) override;

    virtual void DisplayOverallStats() override {};
    ~AppController() override {};
};

#endif