#include <fstream>
#include <thread>
#include "AppController.h"
#include "ImportDatabase.h"
#include "IDisplayStats.h"


void AppController::ImportDatabaseBackground(string csvFilenameToImport, int timeToTruncNewEntries)
{
    std::thread t(&AppController::ImportDatabase, this, csvFilenameToImport, timeToTruncNewEntries);
    printf("starting a thread\n");
    t.detach();
}

void AppController::ImportDatabase(string csvFilenameToImport, int timeToTruncNewEntries)
{
    //make a backup, just in case

    std::string dbFilename = _audioDB->GetDbFilename();
    std::string backupDbFilename = "backup_" + dbFilename;

    std::ifstream  src(dbFilename, std::ios::binary);
    std::ofstream  dst(backupDbFilename,   std::ios::binary);

    dst << src.rdbuf();

    time_t startTime = time(0);
    printf("\nImporting;\nStart time: %s\n\n", ctime(&startTime));

    std::vector<musicDb::ImportedEntry> importedEntries = musicDb::ImportDatabase::ImportDatabaseToMemory(csvFilenameToImport);

    //these variables are a small hack to send a progress to gui app without duplicating too much code
    _b_ImportInProgress = true;
    _b_EntriesToImportTotal = importedEntries.size();
    _b_EntriesProceed = 0;

    time_t previousEntryTime = 0;
    int duplicatesCount = 0;
    int entriesImported = 0;

    int count = 0;

    _audioDB->StartTransaction();
    
    for(musicDb::ImportedEntry parsedEntry: importedEntries)
    {
        if((parsedEntry.unixTime - previousEntryTime) > timeToTruncNewEntries){
            printf("Track ♪%s♪ from album ♪%s♪\n played at %s %s (%i);\n artist: '%s'\n\n", 
                parsedEntry.trackName.c_str(),
                parsedEntry.albumName.c_str(),
                parsedEntry.date.c_str(), parsedEntry.time.c_str(),
                parsedEntry.unixTime,
                parsedEntry.artistName.c_str()
            );
            _audioDB->ImportEntry(parsedEntry);
            entriesImported++;
        } else 
        {
            printf("Got truncated entry: %s: %s, skipping\n\n", parsedEntry.artistName.c_str(), parsedEntry.trackName.c_str());
            duplicatesCount++;
        }

        previousEntryTime = parsedEntry.unixTime;

        //these variables are a small hack to send progress to gui app without duplicating too much code
        _b_EntriesProceed++;
        _b_CurrentEntryToImport = parsedEntry;

        count++;
    };

    _audioDB->CommitTransaction();


    time_t stopTime = time(0);
    time_t timeElapsed = stopTime - startTime;

    printf("Start time: %s\n\n", ctime(&startTime));
    printf("Stop time: %s\n\n", ctime(&stopTime));
    printf("Time to import: %i seconds; %i minutes\n\n", timeElapsed, (int)(timeElapsed/60.0));
    printf("Entries imported: %i \nDuplicated entries skipped: %i\n\n", entriesImported, duplicatesCount);

    ImportDatabaseComplete((int)timeElapsed);
    _b_ImportInProgress = false;
}

bool AppController::GetImportStatus(musicDb::ImportedEntry & parsedEntry, int & currentImportPosition, int & TotalImportCount)
{
    if(_b_ImportInProgress)
    {
        parsedEntry = _b_CurrentEntryToImport;
        currentImportPosition = _b_EntriesProceed;
        TotalImportCount = _b_EntriesToImportTotal;
    } else 
    {
        currentImportPosition = 0;
        TotalImportCount = 0;
    }
    return _b_ImportInProgress;
}

void AppController::ImportDatabaseComplete(int timeElapsed)
{
    for(auto worker : _displayWorkers)
    {
        worker->ImportDatabaseComplete(timeElapsed);
    }   
}

void AppController::DisplayTopAlbums(time_t timeFrom, time_t timeTo)
{
    std::vector<std::pair<musicDb::album, int>> topAlbums = _audioDB->TopAlbumsInInterval(timeFrom, timeTo);
    for(auto worker : _displayWorkers)
    {
        worker->DisplayTopAlbums(topAlbums);
    }   
}

void AppController::DisplayTopTracks(time_t timeFrom, time_t timeTo)
{
    std::vector<std::pair<musicDb::track, int>> topTracks = _audioDB->TopTracksInInterval(timeFrom, timeTo);
    for(auto worker : _displayWorkers)
    {
        worker->DisplayTopTracks(topTracks);
    }
}

void AppController::DisplayTopArtists(time_t timeFrom, time_t timeTo)
{
    std::vector<std::pair<musicDb::artist, int>> topArtists = _audioDB->TopArtistsInInterval(timeFrom, timeTo);
    for(auto worker : _displayWorkers)
    {
        worker->DisplayTopArtists(topArtists);
    }    
}

void AppController::DisplayStatsAll()
{
    this->DisplayTopAlbums(0, time(0));
    this->DisplayTopArtists(0, time(0));
    this->DisplayTopTracks(0, time(0));
    this->DisplayInfo("All Time Stats");
}

void AppController::DisplayStats(TimeIntervalToDisplay intervalToUse)
 {
    time_t timeToUseFrom, timeToUseTo;
    string messageToDisplay = "";
    
    auto timeToString = [](time_t timeToUse) -> std::string
    {
        struct tm * timeinfo;
        timeinfo = localtime ( &timeToUse);
        return std::string(
            std::to_string(timeinfo->tm_mday) + "/" +
            std::to_string(timeinfo->tm_mon + 1) + "/" +
            std::to_string(timeinfo->tm_year + 1900)
        );
    };

    switch (intervalToUse.interval)
    {
    case TimeInterval::custom:
        {
            timeToUseFrom = intervalToUse.timeFrom.GetDateStartTime();
            timeToUseTo = intervalToUse.timeTo.GetDateEndTime();

            messageToDisplay = "Custom date Stats: " 
                + timeToString(timeToUseFrom) + " : "
                + timeToString(timeToUseTo)
                ;

            printf("Display dates are: %i::%i\n\n", timeToUseFrom, timeToUseTo);
        }
        break;

    case TimeInterval::year:
        {
            int monthDuration = _secondsPerDay * 30;
            int yearDuration = monthDuration * 12;
            timeToUseTo = time(0);
            timeToUseFrom = timeToUseTo - yearDuration;
            messageToDisplay = "Last 30 Days Stats";
        }
        break;

    case TimeInterval::month:
        {
            int monthDuration = _secondsPerDay * 30;
            timeToUseTo = time(0);
            timeToUseFrom = timeToUseTo - monthDuration;
            messageToDisplay = "Last 30 Days Stats";
        }
        break;

    case TimeInterval::week:
        {
            int weekDuration = _secondsPerDay * 7;
            timeToUseTo = time(0);
            timeToUseFrom = timeToUseTo - weekDuration;
            messageToDisplay = "Last 7 Days Stats";
        }
        break;

    case TimeInterval::weekBefore:
        {
            int weekDuration = _secondsPerDay * 7;
            timeToUseTo = time(0) - weekDuration;
            timeToUseFrom = timeToUseTo - weekDuration;
            messageToDisplay = "Previous Week Stats";
        }
        break;

    
    default:
        break;
    }

    printf("Time to use from: %s to: %s\n", timeToString(timeToUseFrom).c_str(),timeToString(timeToUseTo).c_str());

    this->DisplayTopAlbums(timeToUseFrom, timeToUseTo);
    this->DisplayTopArtists(timeToUseFrom, timeToUseTo);
    this->DisplayTopTracks(timeToUseFrom, timeToUseTo);
    this->DisplayInfo(messageToDisplay);


 }


void AppController::DisplayInfo(std::string info)
{
    for(auto worker : _displayWorkers)
    {
        worker->DisplayInfo(info);
    }  
}