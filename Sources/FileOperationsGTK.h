#ifndef GTKMM_FileOperationsGTK_H
#define GTKMM_FileOperationsGTK_H

#include <gtkmm.h>
#include "IAppController.h"

class FileOperations
{
    private:
        Gtk::Window * _popupWindowToWorkWith;
        Gtk::Button * _buttonToWorkWith;

    public:
        static void OpenDatabase(IAppController * controllerToUse, Gtk::FileChooserDialog * popupFileDialogWindow, Gtk::Button * buttonThatCalled);
        static void ImportEntriesToCurrentDatabase(IAppController * controllerToUse, std::string csvFilename, int secondsToTruncEntriesToImport);
};

#endif /* GTKMM_FileOperationsGTK_H */
