#include "FileOperationsGTK.h"

void FileOperations::OpenDatabase(IAppController * controllerToUse, Gtk::FileChooserDialog * popupFileDialogWindow, Gtk::Button * buttonThatCalled)
{
    std::string dbFilename = popupFileDialogWindow->get_filename();
    printf("filename chosen is: %s\n\n", dbFilename.c_str());

    controllerToUse->Init(dbFilename);
    controllerToUse->DisplayStats(TimeIntervalToDisplay{TimeInterval::month});
    popupFileDialogWindow->hide(); 
}

void FileOperations::ImportEntriesToCurrentDatabase(IAppController * controllerToUse, std::string csvFilename, int secondsToTruncEntriesToImport)
{
    printf("csv filename chosen is: %s\n\n", csvFilename.c_str());
//    controllerToUse->ImportDatabase(csvFilename, secondsToTruncEntriesToImport);
    controllerToUse->ImportDatabaseBackground(csvFilename, secondsToTruncEntriesToImport);
}