#include <iostream>
#include <string>
#include "../Sqlite3/sqlite3.h"
  
int main_old(int argc, char** argv)
{
    sqlite3* DB;
  
    std::string sql = "CREATE TABLE PERSON("
                        "ID     INT PRIMARY KEY NOT NULL,"
                        "NAME   TEXT    NOT NULL,"
                        "SURNAME    TEXT    NOT NULL,"
                        "AGE    INT         NOT NULL,"
                        "ADDRESS            CHAR(50),"
                        "SALARY             REAL );"
    
    ;
  
    int exit = 0;
    exit = sqlite3_open("example.db", &DB);

    if (exit) {
        std::cerr << "Error open DB " << sqlite3_errmsg(DB) << std::endl;
        return (-1);
    }
    else
        std::cout << "Opened Database Successfully!" << std::endl;


    char * errorMessage;
    exit = sqlite3_exec(DB, sql.c_str(), NULL, 0, &errorMessage);

    if(exit != SQLITE_OK){
        std::cerr << "Error Create Table" << std::endl;
        sqlite3_free(errorMessage);
    } else
        std::cout << "Table created Successfully!" << std::endl;

    sqlite3_close(DB);
    return (0);
}
