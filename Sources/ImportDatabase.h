#ifndef ImportDatabase_H
#define ImportDatabase_H

#include <iostream>
#include <string>
#include <vector>
#include "AudiostatsDatabase.h"

using std::string;

namespace musicDb 
{
    namespace ImportDatabase
    {
            std::vector<ImportedEntry> ImportDatabaseToMemory(std::string csvFilename, int timeTillNextTrackThreshhold = 0);
            int ImportDatabaseToDatabase(std::string csvFilename, AudiostatsDatabase & db, int timeTillNextTrackThreshhold = 0);
            std::vector<string> ParseString(const string & lineToParse, const string delimiter);
            ImportedEntry StringToImportedEntry(const std::vector<string> & stringToParse);
    };
}

#endif