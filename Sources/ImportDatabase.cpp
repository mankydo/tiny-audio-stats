#include <fstream>
#include "ImportDatabase.h"

std::vector<musicDb::ImportedEntry> musicDb::ImportDatabase::ImportDatabaseToMemory(std::string csvFilename, int timeTillNextTrackThreshhold)
{
    std::fstream csvFile;
    std::vector<ImportedEntry> ImportedEntries = std::vector<ImportedEntry>();

    std::vector<string> tempStringVector;

    std::ifstream ifile(csvFilename);
    if(ifile){
        csvFile.open(csvFilename, std::ios::in);
        if(csvFile.is_open()){
            std::string newLine;
            while(std::getline(csvFile, newLine)){

                //file reading here
                if(newLine != "")
                {
                    tempStringVector = ParseString(newLine, ";");
                    ImportedEntries.push_back( StringToImportedEntry(tempStringVector));
                }
            }
        }
    }
    return ImportedEntries;
}

//int ImportDatabaseToDatabase(std::string csvFilename, AudiostatsDatabase & db, int timeTillNextTrackThreshhold = 0);

std::vector<string> musicDb::ImportDatabase::ParseString(const string & lineToParse, const string delimiter){

//    printf("\nGot on input: %s and delimiter %s\n", lineToParse.c_str(), delimiter.c_str());

    size_t pos_start = 0;
    size_t pos_end = 0;
    size_t delimiter_length = delimiter.length();

    int i = 0;
    int limitFieldsTo = 10;

//    printf("length is %i\n", lineToParse.length());


    std::vector<string> result = std::vector<string>();
    string foundStr = "";
    // a stupid hack to simplify logic of getting the last object in a line
    string lineToParseFinal = lineToParse + delimiter;

    while ( (pos_end = lineToParseFinal.find(delimiter, pos_start)) != string::npos && i< limitFieldsTo)
    {

//        printf("position is %i\n", pos_end);
        if(pos_end <= lineToParse.length()) 
        {
//            printf("got something at position %i\n", pos_end);
            result.push_back(lineToParseFinal.substr(pos_start, pos_end - pos_start));
//            printf("got %s at position %i\n", result.back().c_str(), pos_end);
        }

        pos_start = pos_end + delimiter_length;
        i++;
    }

//    printf("end of parsing;\n");
    
    return result;
}

musicDb::ImportedEntry musicDb::ImportDatabase::StringToImportedEntry(const std::vector<string> & stringToParse)
{
    ImportedEntry result;

    //since we know the order in which fields are situated we can hardcode it for now
    // (although it's better to device some sort of mechanism to rearrange those on input)

    //current format is:
    //  0               ; 1           ; 2    ; 3   ; 4
    // date(dd/mm/yyyy);time(hh:mm:ss);artist;album;track


    time_t rawtime;
    struct tm * timeinfo;

    time(&rawtime);
    timeinfo = gmtime(&rawtime);

//    printf("current year is %i, month is %i and hour is %i; unix timestamp: %i\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_hour, rawtime);
//    printf("string first is: %s\n", stringToParse.at(0).c_str());

    auto CheckIfExistsAt = [](std::vector<string> vectorToCheck, int position) -> std::string {
        std::string result = "";
//        printf("trying to parse a string\n");
        try
        {
            result = vectorToCheck.at(position);
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            printf("There was an error in retrieving a value from a parsed string, statring with %s\n", vectorToCheck.at(0).c_str());
        }
        return result;
    };

    if(stringToParse.size() > 0){
        std::vector<string> dateString = ParseString(stringToParse.at(0), "/");
        std::vector<string> timeString = ParseString(stringToParse.at(1), ":");

        timeinfo->tm_mday = stoi(dateString.at(0));
        timeinfo->tm_mon = stoi(dateString.at(1)) - 1;
        timeinfo->tm_year = stoi(dateString.at(2)) - 1900;
        timeinfo->tm_hour = stoi(timeString.at(0));
        timeinfo->tm_min = stoi(timeString.at(1));
        timeinfo->tm_sec = stoi(timeString.at(2));

        time_t unixTime = timegm(timeinfo);

    //    printf("Imported year is %i, month is %i and hour is %i; unix timestamp: %i\n", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_hour, unixTime);

        result.date = stringToParse.at(0);
        result.time = stringToParse.at(1);
        result.unixTime = unixTime;
/*         result.artistName = stringToParse.at(2);
        result.albumName = stringToParse.at(3);
        result.trackName = stringToParse.at(4); */
        result.artistName = CheckIfExistsAt(stringToParse, 2);
        result.albumName = CheckIfExistsAt(stringToParse, 3);
        result.trackName = CheckIfExistsAt(stringToParse, 4);
    }

    return result;
}
