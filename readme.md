# Tiny Audio Stats v 0.5

It's a simple one trick pony c++ program for storing and showing stats of music listened and stored in .csv file.

It consists of two parts:
* a "backend": database, both importer and a basic API for accessing the database
* a "frontend": gtk3 and console frontends for displaying and in case of gtk3 part selecting different options and data provided by API

# Basic Usage

The program uses specifically formated .csv file to import entries into selected(or newly created) database and then a user can use gtk3 frontend to filter data the way it seems fit and allowed by the program.

## CSV format:
the imported .csv file should be formated as such:

_date;time;ArtistName;AlbumName;TrackName;_

Where:
* _date_ format is: _dd/mm/yyyy_
* _time_ format is: hh:mm:ss

# Importing data:
For importing data a user should use two command line options:

$: ./tinyaudiostats _f "filename" db "dbfilename"_

where:
* f "filename": a file name of .csv file to import(at the moment any entry that happened lesse than 10 seconds after a previous one will be ignored to exclude songs that were skipped during listening)
* db "dbfilename": a database file to operate on; if no such filename exists it will be created; if no database name is provided the program won't start at all.

# Other Options:
$: ./tinyaudiostats -other-arguments- _cli_

Using a "cli" argument will force the program to run in cli mode effectively working as an importer-only.


# To Do

* [ ]   Make a separate option for defining an interval for ignoring skipped entries(atm it's 10 seconds for conclusive tracks but it should be user-definable) 
* [ ]   Save last opened database so the program can be started from gui

## Foot notes:
* Dependencies: the program depends only on gtk3 atm; mysqlite is used as an embedded library.
* I've built the program only on linux but since it's gtk3 it should be possible to build it on windows and mac as well.



