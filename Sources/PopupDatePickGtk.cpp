#include "PopupDatePickGtk.h"

void PopupDatePickGtk::ShowPopup(musicDb::DayDate * dateToReturnTo, Gtk::Window * popupWindow, Gtk::Button * buttonThatCalled)
{
    //To get a desired window position we need to get a button position inside a window and add a window position to it:    
    gint wx=0, wy=0;
    Gtk::Allocation alloc;
    alloc = buttonThatCalled->get_allocation();
    wx = alloc.get_x();
    wy = alloc.get_y();

    Gtk::Container * conPtr = buttonThatCalled->get_toplevel();
    gint winX, winY;
    dynamic_cast<Gtk::Window *>(conPtr)->get_position(winX, winY);
    wx = wx + winX;
    wy = wy + winY + alloc.get_height() + 30;

    printf("Name of the parent is: %s and title is: %s\n", conPtr->get_name().c_str(), dynamic_cast<Gtk::Window *>(conPtr)->get_title().c_str());
    printf("Parent position is: %i::%i\n", winX, winY);

    popupWindow->move(wx, wy);
    popupWindow->present();
    popupWindow->activate_focus();
    popupWindow->grab_focus();
    popupWindow->get_child()->grab_focus();

    _popupOpenTime = time(0);

    _popupWindowToWorkWith = popupWindow;
    _buttonToWorkWith = buttonThatCalled;
    _dateToReturnTo = dateToReturnTo;


    for(auto child : popupWindow->get_children()){
        printf("child name is: %s\n",child->get_name().c_str() );
        if(child->get_name() == "popup_Calendar")
        {
            printf("Got calendar to work with\n");
            _calendarToWorkWith = dynamic_cast<Gtk::Calendar *>(child);
            _calendarToWorkWith->select_month(dateToReturnTo->GetMonth(), dateToReturnTo->GetYear());
            _calendarToWorkWith->select_day(dateToReturnTo->GetDay());
        }
    }

    _signalConnect_FocusOut.disconnect();
    _signalConnect_DayChosen.disconnect();
    _signalConnect_MonthChosen.disconnect();

    if(_calendarToWorkWith != nullptr)
    {
        _signalConnect_DayChosen = _calendarToWorkWith->signal_day_selected().connect(
            [this]()
            {
                printf("day selected!\n");
                if(time(0) >= this->_monthChangedAt + 1 )
                {
                    printf("Changing a day and closing!\n\n");
                    ClosePopup(true);
                }
            }
        );
        _signalConnect_MonthChosen = _calendarToWorkWith->signal_month_changed().connect(
            [this]()
            {
                printf("month changed!\n");
                this->_monthChangedAt = time(0);
            }
        );
    } else {
        throw std::runtime_error("Error opening calendar selector; exiting!\n");
    }

    _signalConnect_FocusOut = popupWindow->signal_focus_out_event().connect(
        [this](bool inStatus) -> bool
        {
            time_t timeNow = time(0);
            printf("time of openin popup is: %i and time now is %i: \n", this->_popupOpenTime, timeNow);

            if(timeNow >= this->_openedWindowTimeout + this->_popupOpenTime){
                ClosePopup(false);
            }
            return true;
        }
    );

    printf("Window position should be: %i::%i\n", wx, wy);
}

void PopupDatePickGtk::ClosePopup(bool returnResults)
{
    printf("Pop goes out!\n");
    if(returnResults)
    {
        if(_buttonToWorkWith != nullptr){
            guint day, month, year;
            time_t unixTime;
            _calendarToWorkWith->get_date(year, month, day);

            //here to get unix times from selected:
            // from is: date + 0h0m to set it to the beginning of the day
            // to is: date + 23h59m to set it to the end of the day

        printf("returnun after calendar\n");

            _buttonToWorkWith->set_label(std::to_string(day) + "/" +
                std::to_string(month + 1) + "/" +
                std::to_string(year)
            );

            _dateToReturnTo->SetTimes(day, month, year); 
        printf("returnun after daetes\n");
        }
    }


    // disconnect signals here:
        printf("returnun after checks\n");

    _signalConnect_FocusOut.disconnect();
    _signalConnect_DayChosen.disconnect();
    _signalConnect_MonthChosen.disconnect();

        printf("returnun after disconects\n");

    _popupWindowToWorkWith->hide();
//    delete this;

    printf("Popup window is dying\n");
}
