	
CXX=g++
CFLAGS=-g -lpthread -ldl -Wall -Wextra -Werror -no-pie
INCLUDE=-I
SRCDIR=Sources/
OBJDIR=obj/
SOURCES=$(wildcard $(SRCDIR)*.cpp)
SQLITE_DIR=Sqlite3/
DEPS = 

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CXX) -c $< -o $@ -I$(SQLITE_DIR) `pkg-config gtkmm-3.0 --cflags`
	@echo "\n sources are: " $(SOURCES)
	@echo "\n get arg: "$@" and "$<""
	@echo "   Object created:" $@
	@echo "\n"

tinyaudiostats: $(SOURCES:$(SRCDIR)%.cpp=$(OBJDIR)%.o)
	$(CXX) -o tinyaudiostats $^ $(SQLITE_DIR)sqlite3.o -I$(SQLITE_DIR) `pkg-config gtkmm-3.0 --libs` $(CFLAGS)
	@echo "\nCompiled tiny audio stats successfuly!"



