#include <fstream>
#include "AudiostatsDatabase.h"

musicDb::AudiostatsDatabase::AudiostatsDatabase(string dBfilename): BaseDatabase(dBfilename)
{
    InitDatabase(_DB);
}

musicDb::AudiostatsDatabase::~AudiostatsDatabase()
{
    sqlite3_close(_DB);
}

std::string musicDb::AudiostatsDatabase::GetDbFilename()
{
    return _dbFilename;
}

int musicDb::AudiostatsDatabase::InitDatabase(sqlite3 * db)
{
    std::vector<std::pair<string, string>> sql_queues;

    sql_queues.push_back(std::make_pair("CREATE TABLE TracksListened("
                        "TrackId     INT NOT NULL,"
                        "UnixTime    INT NOT NULL,"
                        "FOREIGN KEY(TrackId) REFERENCES Tracks(id)"
                        ");"
    , "TracksListned"));

    sql_queues.push_back(std::make_pair("CREATE TABLE Tracks("
                        "id     INTEGER PRIMARY KEY NOT NULL,"
                        "name           TEXT NOT NULL,"
                        "artistId       INT NOT NULL,"
                        "albumId        INT,"
                        "duration       INT,"
                        "year           INT,"
                        "FOREIGN KEY(artistId) REFERENCES Artists(id)"
                        ");"
    , "Tracks"));

    sql_queues.push_back(std::make_pair("CREATE TABLE Artists("
                        "id             INTEGER PRIMARY KEY NOT NULL,"
                        "description    TEXT    NOT NULL,"
                        "photoId        INT"
                        ");"    
    , "Artists"));

    sql_queues.push_back(std::make_pair("CREATE TABLE ArtistsNames("
                        "artistId       INT NOT NULL,"
                        "name           TEXT NOT NULL,"
                        "isDefault      BOOLEAN DEFAULT TRUE,"
                        "FOREIGN KEY(artistId) REFERENCES Artists(id)"
                        ");"
    , "ArtistsNames"));

    sql_queues.push_back(std::make_pair("CREATE TABLE Albums("
                        "id             INTEGER PRIMARY KEY NOT NULL,"
                        "name           TEXT,"
                        "description    TEXT,"
                        "coverId        INT,"
                        "year           INT"
                        ");"
    , "Albums"));

    sql_queues.push_back(std::make_pair("CREATE TABLE AlbumArtists("
                        "albumId       INT NOT NULL,"
                        "artistId       INT NOT NULL,"
                        "FOREIGN KEY(artistId) REFERENCES Artists(id),"
                        "FOREIGN KEY(albumId) REFERENCES Albums(id),"
                        "UNIQUE(albumId,artistId)"
                        ");"
    , "AlbumArtists"));

    sql_queues.push_back(std::make_pair("CREATE TABLE Tags("
                        "id     INTEGER PRIMARY KEY NOT NULL,"
                        "name   TEXT    NOT NULL"
                        ");"
    , "Tags"));

    sql_queues.push_back(std::make_pair("CREATE TABLE TracksTags("
                        "trackId    INT     NOT NULL,"
                        "TagId      INT     NOT NULL,"
                        "FOREIGN KEY(trackId) REFERENCES Tracks(id),"
                        "FOREIGN KEY(TagId) REFERENCES Tags(id)"
                        ");"    
    , "TracksTags"));

    sql_queues.push_back(std::make_pair("CREATE TABLE Images("
                        "id     INTEGER PRIMARY KEY NOT NULL,"
                        "data   BLOB );"   
    , "Images"));


    int exit = 0;
    char * errorMessage;

    auto sql_execute= [](sqlite3 * dbIn, char * error, string sql, string sqlName)
    {
        int exit;
        exit = sqlite3_exec(dbIn, sql.c_str(), NULL, 0, &error);

        if(exit != SQLITE_OK){
            printf("Error Create Table: %s\n", sqlName.c_str());
            sqlite3_free(error);
        } else
            printf("Table '%s' created Successfully!\n", sqlName.c_str());

        return exit;
    };

    for(std::pair<string, string> sql_queue: sql_queues){
        sql_execute(db, errorMessage, sql_queue.first, sql_queue.second);
    }

    printf("Init db complete\n");

    return 1;
}

int musicDb::AudiostatsDatabase::ImportEntry(BaseEntry & entryToImport)
{

    ImportedEntry * importedEntry = (ImportedEntry *)&entryToImport;

//    printf("got a track named: %s\n", importedEntry->trackName.c_str());

    // import track:
    // first find an artist and get its ID; if not found add a new artist and get that ID instead
    // find an album for this artist, if no album with this name create new; 
    //      if no artist was found skip to creating new Album
    // find a track with that name and with artist and album ID; 
    //      if not found add a new track
    // add timestamp and track ID to trackListened table

    int artistId =  GetArtistId(importedEntry->artistName);

    if(artistId == DbErrors::notfound)
        artistId = AddArtistToDb(importedEntry->artistName);

    int trackId = 0;

    if(importedEntry->albumName != "") 
    {
        int albumId = GetAlbumId(importedEntry->albumName);

        if(albumId == DbErrors::notfound) {
            printf("trying to add an album\n");
            albumId = AddAlbumToDb(artistId, importedEntry->albumName);
        } else {
            printf("trying to add an artist to an album\n");
            AddArtistToAlbum(artistId, albumId);
        }

        trackId = GetTrackId(importedEntry->trackName, artistId, albumId);

        if(trackId == DbErrors::notfound)
            trackId = AddTrackToDb(importedEntry->trackName, artistId, albumId);
    } 
        else 
    {
        trackId = GetTrackId(importedEntry->trackName, artistId);

        if(trackId == DbErrors::notfound)
            trackId = AddTrackToDb(importedEntry->trackName, artistId);
    }


    //[future improvement]: add a track to a vector and perform one big insert in 100 or so steps
    int trackAddedStatus = AddScrobbledTrack(trackId, importedEntry->unixTime);
    if(trackAddedStatus == 0){
        printf("Track %s was added successfuly;\n", string(importedEntry->artistName + ": " + importedEntry->trackName).c_str() );
    }
 
    return 1;
}

int musicDb::AudiostatsDatabase::GetArtistId(string artistName)
{
    int result = DbErrors::notfound;
    std::vector<string> row = GetFirstRowOfSelect("select artistId from ArtistsNames where name = \"" + artistName + "\";");

    if(row.size() > 0)
        result = stoi(row.at(0));

    return result;
}        

int musicDb::AudiostatsDatabase::GetAlbumId(string albumName, int artistId)
{
    int result = DbErrors::notfound;
    std::vector<string> row = GetFirstRowOfSelect(
        "select id from Albums, AlbumArtists where Albums.name = \"" 
        + albumName 
        + "\" and artistId='"
        + std::to_string(artistId)
        + "' "
        + " and Albums.id = AlbumArtists.albumId"
        + ";");

    if(row.size() > 0)
        result = stoi(row.at(0));

    return result;
} 

int musicDb::AudiostatsDatabase::GetAlbumId(string albumName)
{
    int result = DbErrors::notfound;
    std::vector<string> row = GetFirstRowOfSelect(
        "select id from Albums where Albums.name = \"" 
        + albumName 
        + "\" ;");

    if(row.size() > 0)
        result = stoi(row.at(0));

    return result;
} 

int musicDb::AudiostatsDatabase::GetTrackId(string trackName, int artistId, int albumId)
{
    int result = DbErrors::notfound;
    std::vector<string> row = GetFirstRowOfSelect(
        "select id from Tracks where name = \"" 
        + trackName 
        + "\" and artistId='"
        + std::to_string(artistId)
        + "' and albumId='"
        + std::to_string(albumId)
        + "';");

    if(row.size() > 0)
        result = stoi(row.at(0));

    return result;
} 

int musicDb::AudiostatsDatabase::GetTrackId(string trackName, int artistId)
{
    int result = DbErrors::notfound;
    std::vector<string> row = GetFirstRowOfSelect(
        "select id from Tracks where name = \"" 
        + trackName 
        + "\" and artistId='"
        + std::to_string(artistId)
        + "';");

    if(row.size() > 0)
        result = stoi(row.at(0));

    return result;
} 


int musicDb::AudiostatsDatabase::AddArtistToDb(string artistName)
{
    int errors = 0;

    string queue = "insert into Artists(description) values(''); ";
    int artistId = getLastRowIdFromTable("Artists");
    
    errors =  ExecuteSql(queue);

    if(errors == SQLITE_OK) {
        queue = "insert into ArtistsNames(artistId, name) values(" 
            + std::to_string(artistId)
            + ", \"" + artistName + "\""
            + ")";
    }

    errors = ExecuteSql(queue);

    return artistId;
}

int musicDb::AudiostatsDatabase::AddAlbumToDb(int artistId, string albumName)
{
    //insert an album to Albums
    // then insert artist to AlbumArtists 

    int errors = 0;

    int albumId = getLastRowIdFromTable("Albums") + 1;
    printf("album id is: %i\n", albumId);

    string queue = "insert into Albums(id, name) values("
        + std::to_string(albumId)
        + ", \"" 
        + albumName + "\""
        +"); ";

    queue += " insert into AlbumArtists(albumId, artistId) values("
        + std::to_string(albumId)
        + ", "
        + std::to_string(artistId)
        +"); ";

    errors = ExecuteSql(queue);

    return albumId;
}

int musicDb::AudiostatsDatabase::AddArtistToAlbum(int artistId, int albumId)
{
    int errors = 0;



    string queue = " insert into AlbumArtists(albumId, artistId) values("
        + std::to_string(albumId)
        + ", "
        + std::to_string(artistId)
        +"); ";

    errors = ExecuteSql(queue);

    return albumId;
}

int musicDb::AudiostatsDatabase::AddTrackToDb(string trackName, int artistId, int albumId)
{
    int errors = 0;
    string queue = "insert into Tracks(name, artistId, albumId) values(\""
        + trackName 
        + "\", "
        + std::to_string(artistId)
        + ", "
        + std::to_string(albumId)
        +"); ";

    errors = ExecuteSql(queue);

    int trackId = getLastRowIdFromTable("Tracks");

    return trackId;
}

int musicDb::AudiostatsDatabase::AddTrackToDb(string trackName, int artistId)
{
    int errors = 0;
    string queue = "insert into Tracks(name, artistId) values(\""
        + trackName 
        + "\", "
        + std::to_string(artistId)
        +"); ";

    errors = ExecuteSql(queue);

    int trackId = getLastRowIdFromTable("Tracks");

    return trackId;
}

int musicDb::AudiostatsDatabase::AddScrobbledTrack(int trackId, time_t unixTime)
{
    int errors = 0;

    std::vector<string> trackListened = GetFirstRowOfSelect("select * from TracksListened where UnixTime =" + std::to_string(unixTime) + ";");
    if(trackListened.size() == 0)
    {

        string queue = "insert into TracksListened(TrackId, UnixTime) values("
            + std::to_string(trackId)
            + ", "
            + std::to_string(unixTime)
            +"); ";

        ExecuteSql(queue);
    } 
    else {
        errors = -1;
    }

    return errors;
}