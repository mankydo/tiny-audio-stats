#include "DisplayWorkerGtk.h"
#include "FileOperationsGTK.h"

/* //Because there is no make_managed in my system-wide gtkmm :((((
namespace Gtk
{
        template<class T, class... T_Args>
        auto make_managed(T_Args&&... args) -> T*
        {
          return manage(new T(std::forward<T_Args>(args)...));
        }
} */

DisplayWorkerGtk::DisplayWorkerGtk()
: Gtk::Application("org.my.tinyaydiostats", Gio::APPLICATION_HANDLES_OPEN)
{
}

Glib::RefPtr<DisplayWorkerGtk> DisplayWorkerGtk::create()
{
        return Glib::RefPtr<DisplayWorkerGtk>(new DisplayWorkerGtk());
}

Glib::RefPtr<DisplayWorkerGtk> DisplayWorkerGtk::create(IAppController * appControllerToUse)
{
        return Glib::RefPtr<DisplayWorkerGtk>(new DisplayWorkerGtk(appControllerToUse));
}


Gtk::ApplicationWindow* DisplayWorkerGtk::create_appwindow()
{
  //  auto appwindow = new ExampleAppWindow();
      ui = Gtk::Builder::create_from_file("tinyaudostats.glade");
      Gtk::ApplicationWindow * appwindow = nullptr;
      ui->get_widget<Gtk::ApplicationWindow>("mainWindow", appwindow);
      mainWindow = appwindow;
      appwindow->show_all(); 

      ui->get_widget<Gtk::Box>("TopTracks", boxForTracks);
      ui->get_widget<Gtk::Box>("TopArtists", boxForArtists);
      ui->get_widget<Gtk::Box>("TopAlbums", boxForAlbums);
      ui->get_widget<Gtk::Label>("infoLabel", infoLabel);

      dateFrom.SetTimes(time(0));
      dateTo.SetTimes(time(0));

      //load css
      Glib::RefPtr<Gtk::CssProvider> cssProvider = Gtk::CssProvider::create();
      cssProvider->load_from_path("tinyaudostats.css");
      
      Glib::RefPtr<Gtk::StyleContext> styleContext = Gtk::StyleContext::create();
      
      //get default screen
      Glib::RefPtr<Gdk::Screen> screen = Gdk::Screen::get_default();
      
      //add provider for screen in all application
      styleContext->add_provider_for_screen(screen, cssProvider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);


      // Make sure that the application runs for as long this window is still open.
      add_window(*appwindow);

      // Delete the window when it is hidden.
      appwindow->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(*this,
        &DisplayWorkerGtk::on_hide_window), appwindow));

      Gtk::Button * buttonPtr = nullptr;
      ui->get_widget<Gtk::Button>("close_button", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::mem_fun(mainWindow, &Gtk::ApplicationWindow::hide));  

      // waay too much boilerplate code so it's been wrapped into a separate method
      FileOperationsGUIBuilder();

      ui->get_widget<Gtk::Button>("button_AllTime", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::mem_fun(_appController, &IAppController::DisplayStatsAll));  


      ui->get_widget<Gtk::Button>("button_Last7days", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::bind(
        sigc::mem_fun(_appController, &IAppController::DisplayStats), 
        TimeIntervalToDisplay{TimeInterval::week})
      );  


      ui->get_widget<Gtk::Button>("button_Last30days", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::bind(
        sigc::mem_fun(_appController, &IAppController::DisplayStats), 
        TimeIntervalToDisplay{TimeInterval::month})
      ); 


      auto setDateToButton = [](Gtk::Button * buttonPtr)
      {
        time_t currentTime = time(0);
        struct tm * timeinfo;
        timeinfo = localtime(&currentTime);

        buttonPtr->set_label(
          std::to_string(timeinfo->tm_mday) + "/" +
          std::to_string(timeinfo->tm_mon + 1) + "/" +
          std::to_string(timeinfo->tm_year + 1900)
        );
      };


      Gtk::Window * popoverPtr = nullptr;
      ui->get_widget<Gtk::Window>("popup_DateFrom", popoverPtr);

      Gtk::Button * buttonFromPtr = nullptr;
      ui->get_widget<Gtk::Button>("button_DateFrom", buttonFromPtr);
      buttonFromPtr->signal_clicked().connect(
          [=]()
          {
            _popupCalendarWindow.ShowPopup(&this->dateFrom, popoverPtr, buttonFromPtr);
          }
        );  

      setDateToButton(buttonFromPtr);
    
      Gtk::Button * buttonToPtr = nullptr;
      ui->get_widget<Gtk::Button>("button_DateTo", buttonToPtr);
      setDateToButton(buttonToPtr);

      buttonToPtr->signal_clicked().connect(
          [=]()
          {
            _popupCalendarWindow.ShowPopup(&this->dateTo, popoverPtr, buttonToPtr);
          }
        ); 


      ui->get_widget<Gtk::Button>("button_CustomPeriod", buttonPtr);
      buttonPtr->signal_clicked().connect(
        [this]()
        {
          printf("Dates to use are d/m/y: %i: %i: %i\n", this->dateFrom.GetDay(),this->dateFrom.GetMonth(),this->dateFrom.GetYear());
          printf("Dates to use are: %i, %i\n\n", this->dateFrom.GetDateStartTime(), this->dateTo.GetDateEndTime());
          this->_appController->DisplayStats(TimeIntervalToDisplay{TimeInterval::custom, this->dateFrom, this->dateTo});
        }
      );  

      printf("Done creating window\n");

      _appController->DisplayStats(TimeIntervalToDisplay{TimeInterval::week});

      return appwindow;
}

void DisplayWorkerGtk::on_open(const Gio::Application::type_vec_files& files,
  const Glib::ustring& /* hint */)
{
    // The application has been asked to open some files,
    // so let's open a new view for each one.
    Gtk::ApplicationWindow* appwindow = nullptr;
    auto windows = get_windows();
    if (windows.size() > 0)
      appwindow = dynamic_cast<Gtk::ApplicationWindow*>(windows[0]);

    if (!appwindow)
      appwindow = create_appwindow();

    appwindow->present();
}

void DisplayWorkerGtk::on_activate()
{
    // The application has been started, so let's show a window.
    auto appwindow = create_appwindow();
    appwindow->present();
    
    printf("window activated;\n");
}


void DisplayWorkerGtk::on_hide_window(Gtk::Window* window)
{
  delete window;
}

Gtk::Fixed *DisplayWorkerGtk::CreateEntry(int count, std::string line1, std::string line2, std::string line3, int parentWidth, float fraction_01)
{
//        printf("started creating fixed\n");

        int maximumLabelLength = 70;
        int entryX = 340;
        int entryY = 24;

        Gtk::Label * entryCount = Gtk::make_managed<Gtk::Label>();
        entryCount->set_text(std::to_string(count));
        entryCount->set_name("entryCount_" + std::to_string(count)+"_"+line1);
        entryCount->get_style_context()->add_class("SliderCount");
        entryCount->set_size_request(30,24);
        entryCount->show();

        Gtk::Label * entryBackgroundSlider = Gtk::make_managed<Gtk::Label>();
        entryBackgroundSlider->set_text("");
        entryBackgroundSlider->set_name("entryBackgroundSlider_" + std::to_string(count));
        entryBackgroundSlider->get_style_context()->add_class("SliderBackground");

        int sliderWigth = (float)parentWidth * fraction_01;
        sliderWigth = sliderWigth > entryX ? entryX : sliderWigth;

        Glib::RefPtr<Gtk::CssProvider> cssProvider = Gtk::CssProvider::create();
        std::string cssBody = ".SliderBackground {min-width: "+ std::to_string(sliderWigth) +"px;}";
        cssProvider->load_from_data(cssBody);
        entryBackgroundSlider->get_style_context()->add_provider(cssProvider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

        std::string cssAnimate = ".SliderBackground {"
          "  animation-name: example;"
          "  animation-duration: 2s;"
          " animation-iteration-count: 1;"
          " animation-fill-mode: forwards;"
          " }"
          " @keyframes example {"
          " 0%   {min-width: "+ std::to_string(entryX) +"px;}"          
          "  100% {min-width: "+ std::to_string(sliderWigth) +"px;}"          
          " }";
        cssProvider->load_from_data(cssAnimate);
        entryBackgroundSlider->get_style_context()->add_provider(cssProvider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

        
        entryBackgroundSlider->show();
        

        auto entryLabel = [](std::string line, std::string styleName, int parentWidth, int count, int maxLetterLength, int maxWidth)
        {
            Gtk::Label * entryLine1 = Gtk::make_managed<Gtk::Label>();

            std::string newLine;
            for(int i= 0; i < line.length(); i++){
                std::string str = line.substr(i, 1);
                if(str == "&")
                  newLine += "&amp;";
                else if(str=="<" && line.substr(i+1, 1) != "s" && line.substr(i+1, 1) != "/")
                  newLine += "&lt;";
                else
                  newLine += str;
            }

            entryLine1->set_markup(newLine);
            entryLine1->set_name("entryLine1_" + std::to_string(count)+"_"+line);
            entryLine1->get_style_context()->add_class(styleName);
            entryLine1->set_selectable(true);

            if(newLine.length() > maxLetterLength)
            {
              entryLine1->set_ellipsize(Pango::EllipsizeMode::ELLIPSIZE_END);
              int entryWigth = parentWidth > maxWidth ? maxWidth : parentWidth;
              entryLine1->set_size_request(entryWigth,10);
            }
            entryLine1->show();
            return entryLine1;
        };


        Gtk::Fixed * fixedContainer = Gtk::make_managed<Gtk::Fixed>();
        fixedContainer->set_name("fixed_"+line1+"_"+std::to_string(count));
        fixedContainer->set_size_request(entryX, entryY);
        fixedContainer->put(*entryBackgroundSlider, 4, 0);
        fixedContainer->put(*entryCount, 0, 3);
        fixedContainer->show();

        int line1OffsetY = 0;
        int line2OffsetY = 10;
        int line3OffsetY = 18;
        
        if(line2 == ""){
            line3OffsetY = line2OffsetY + 4;
        } else if(line3 == "") {
            line2OffsetY += 4;
        }

        fixedContainer->put(*entryLabel(line3, "SliderLabelLine3", parentWidth, count, maximumLabelLength - 6, entryX), 50, line3OffsetY);
        fixedContainer->put(*entryLabel(line2, "SliderLabelLine2", parentWidth, count, maximumLabelLength - 3, entryX), 40, line2OffsetY);
        fixedContainer->put(*entryLabel(line1, "SliderMainLabel", parentWidth, count, maximumLabelLength, entryX), 30, line1OffsetY);

//        printf("finished creating fixed\n");

        return fixedContainer;
}

void DisplayWorkerGtk::clearBox(Gtk::Box * boxPointer, std::string prefix)
{
    std::vector<Gtk::Widget *> listOfChilds = boxPointer->get_children();
    for(Gtk::Widget * child : listOfChilds){
//      printf("Prefix is: '%s', prefix length is %i;\n child name is: '%s', substr is '%s'\n",
//       prefix.c_str(), prefix.length(), child->get_name().c_str(), child->get_name().substr(prefix.length()).c_str());
      if(child->get_name().substr(0, prefix.length()) == prefix)
      {
//          printf("child name is: %s\n", child->get_name().c_str());
          boxPointer->remove(*child);
          delete child;
      }
    }
}

void DisplayWorkerGtk::DisplayTopAlbums(std::vector<std::pair<musicDb::album, int>> listOfAlbumsToDisplay)
{
    clearBox(boxForAlbums);
    for(std::pair<musicDb::album, int> line: listOfAlbumsToDisplay)
    {
        int MaxCount = listOfAlbumsToDisplay[0].second;
        Gtk::Fixed * fixedContainer = CreateEntry(
          line.second,
          line.first.name,
          line.first.artistName,
          "",
          boxForAlbums->get_width(), (float)line.second / MaxCount);

        boxForAlbums->pack_start(*fixedContainer);
    }
}

void DisplayWorkerGtk::DisplayTopTracks(std::vector<std::pair<musicDb::track, int>> listOfTracksToDisplay)
{
    clearBox(boxForTracks);
    for(std::pair<musicDb::track, int> line: listOfTracksToDisplay)
    {
        int MaxCount = listOfTracksToDisplay[0].second;
        Gtk::Fixed * fixedContainer = CreateEntry(
          line.second,
          line.first.name,
          line.first.artistName + "<span color='#72a0a5'> from</span> '" + line.first.albumName+"'",
          "",
          boxForTracks->get_width(), (float)line.second / MaxCount);

        boxForTracks->pack_start(*fixedContainer);
    }
}

void DisplayWorkerGtk::DisplayTopArtists(std::vector<std::pair<musicDb::artist, int>> listOfArtistToDisplay)
{
    clearBox(boxForArtists);
    for(std::pair<musicDb::artist, int> line: listOfArtistToDisplay)
    {
        int MaxCount = listOfArtistToDisplay[0].second;
        Gtk::Fixed * fixedContainer = CreateEntry(
          line.second,
          line.first.name,
          "",
          "",
          boxForArtists->get_width(), (float)line.second / MaxCount);

        boxForArtists->pack_start(*fixedContainer);
    }
}

void DisplayWorkerGtk::DisplayInfo(std::string info){
    infoLabel->set_text(info);
    mainWindow->set_title(_applicationName + ": " + info);
}

void DisplayWorkerGtk::FileOperationsGUIBuilder()
{
      Gtk::Button * buttonPtr = nullptr;

      // a method for setting file dialog filters for future use when opening file dialog windows to set it in one place
      auto SetFiltersForFileDialog = [](Gtk::FileChooserDialog * fileDialogToWorkWith, std::string patternName, std::string patternToSet)
      {
          Glib::RefPtr<Gtk::FileFilter> dbFileFilter = Gtk::FileFilter::create();
          dbFileFilter->set_name(patternName);
          dbFileFilter->add_pattern(patternToSet);
          for(auto filter: fileDialogToWorkWith->list_filters())
          {
            fileDialogToWorkWith->remove_filter(filter);
          };
          fileDialogToWorkWith->add_filter(dbFileFilter);
      };


      Gtk::Window * fileDialogWindow = nullptr;
      ui->get_widget<Gtk::Window>("w_FileChooseDialog", fileDialogWindow);
      fileDialogWindow->set_size_request(750,550);

      //binding a button for opening new database
      ui->get_widget<Gtk::Button>("openDatabaseButton", buttonPtr);
      buttonPtr->signal_clicked().connect(
      [=]()
        {
            SetFiltersForFileDialog(dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow),"TinyAudioStats database(*.tasdb)", "*.tasdb");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->set_current_folder(".");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->show();
        }
      );
      //binding a button for opening database file
      Gtk::Button * buttonPtrOk = nullptr;
      ui->get_widget<Gtk::Button>("fileChosenOk", buttonPtrOk);
      buttonPtrOk->signal_clicked().connect(
          [=]()
          {
              std::string filename = dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->get_filename();
              if(filename.length() > 0)
              {
                  if(filename.substr(filename.length() - std::string(".tasdb").length()) == ".tasdb")
                      FileOperations::OpenDatabase(_appController, dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow), buttonPtrOk);
              }
          }
      );
      ui->get_widget<Gtk::Button>("fileChosenCancel", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::mem_fun(fileDialogWindow, &Gtk::ApplicationWindow::hide)); 


      //binding a window to a button for opening new csv file
      ui->get_widget<Gtk::Window>("w_FileChooseDialogCSV", fileDialogWindow);
      
      Gtk::Window * csvWindowDialog = nullptr;
      ui->get_widget<Gtk::Window>("w_fileCsvImport", csvWindowDialog);

      ui->get_widget<Gtk::Button>("addEntriesToDatabaseButton", buttonPtr);
      buttonPtr->signal_clicked().connect(
      [=]()
        {
            SetFiltersForFileDialog(dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow),"TinyAudioStats database import(*.tascsv)", "*.tascsv");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->set_current_folder(".");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->show();
        }
      );

      //binding a button inside a csv import window for a file dialog
      ui->get_widget<Gtk::Button>("csv_fileNameChooseButton", buttonPtr);
      buttonPtr->signal_clicked().connect(
      [=]()
        {
            SetFiltersForFileDialog(dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow),"TinyAudioStats database import(*.tascsv)", "*.tascsv");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->set_current_folder(".");
            dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->show();
            csvWindowDialog->hide();
        }
      );

      sigc::connection m_connectionUpdateProgressBar;


      //On choosing a csv file initialize and set states for csv-import window elements(activating/deactivating those)
      ui->get_widget<Gtk::Button>("fileChosenOk_csv", buttonPtrOk);
      buttonPtrOk->signal_clicked().connect(
      [=]()
          {

              std::string filename = dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->get_filename();
              if(filename.length() > 0)
              {
                  if(filename.substr(filename.length() - std::string(".tascsv").length()) == ".tascsv")
                  {

                      Gtk::Button * buttonPtr_intrl = nullptr;

                      Gtk::Label * csvFileName = nullptr;
                      ui->get_widget<Gtk::Label>("csv_fileName", csvFileName);
                      csvFileName->set_text(dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->get_filename());

                      ui->get_widget<Gtk::Button>("csv_fileNameChooseButton", buttonPtr_intrl);
                      buttonPtr_intrl->set_sensitive(true);

                      Gtk::Scale * scalePtr = nullptr;
                      ui->get_widget<Gtk::Scale>("csv_timeScale", scalePtr);
                      scalePtr->set_sensitive(true);

                      Gtk::Spinner * csvSpinner = nullptr;
                      ui->get_widget<Gtk::Spinner>("csv_importProgressSpinner", csvSpinner);
                      csvSpinner->set_opacity(0);

                      Gtk::ProgressBar * csvProgress = nullptr;
                      ui->get_widget<Gtk::ProgressBar>("csv_importProgress", csvProgress);
                      csvProgress->set_opacity(0);

                      ui->get_widget<Gtk::Button>("csv_importButton", buttonPtr_intrl);
                      buttonPtr_intrl->set_sensitive(true);

                      ui->get_widget<Gtk::Button>("csv_importButtonDone", buttonPtr_intrl);
                      buttonPtr_intrl->set_label("Cancel");
                      buttonPtr_intrl->set_sensitive(true); 

                      fileDialogWindow->hide();

                      Gtk::Window * mainWindowToDisable = nullptr;
                      ui->get_widget<Gtk::Window>("mainWindow", mainWindowToDisable);
                      mainWindowToDisable->set_sensitive(false);

                      csvWindowDialog->show();
                      printf("showing csv window\n\n");
                  }
              }
          }
      );

      ui->get_widget<Gtk::Button>("csv_importButton", buttonPtrOk);
      buttonPtrOk->signal_clicked().connect(
      [=]()
          {
              Gtk::Button * buttonPtr_intrl = nullptr;

              ui->get_widget<Gtk::Button>("csv_fileNameChooseButton", buttonPtr_intrl);
              buttonPtr_intrl->set_sensitive(false);

              Gtk::Scale * scalePtr = nullptr;
              ui->get_widget<Gtk::Scale>("csv_timeScale", scalePtr);
              scalePtr->set_sensitive(false);

              Gtk::Spinner * csvSpinner = nullptr;
              ui->get_widget<Gtk::Spinner>("csv_importProgressSpinner", csvSpinner);
              csvSpinner->set_opacity(100);

              Gtk::ProgressBar * csvProgress = nullptr;
              ui->get_widget<Gtk::ProgressBar>("csv_importProgress", csvProgress);
              csvProgress->set_opacity(100);

              ui->get_widget<Gtk::Button>("csv_importButton", buttonPtr_intrl);
              buttonPtr_intrl->set_sensitive(false);

              ui->get_widget<Gtk::Button>("csv_importButtonDone", buttonPtr_intrl);
              buttonPtr_intrl->set_label("Done");
              buttonPtr_intrl->set_sensitive(false); 

              printf("importing csv file\n\n");
    
              FileOperations::ImportEntriesToCurrentDatabase(_appController, dynamic_cast<Gtk::FileChooserDialog *>(fileDialogWindow)->get_filename(), (int)lround(scalePtr->get_value()));

              Glib::signal_timeout().connect( [=]() 
              {
                  int totalProgress, currentProgress;
                  musicDb::ImportedEntry currentEntry;
//                  printf("!!! progressing now !!!\n");
                  if(_appController->GetImportStatus(currentEntry, currentProgress, totalProgress))
                  {
                    csvProgress->set_fraction(currentProgress / (float)totalProgress);
                    csvProgress->set_text("Current entry to proceed: " + currentEntry.artistName + " : " + currentEntry.trackName);
                    
                    printf("current progress: %i from %i, name: %s\n", currentProgress, totalProgress, (currentEntry.artistName + " : " + currentEntry.trackName).c_str());

                    return true;
                  }
                  else
                    printf("import is completed!\n");
                    csvProgress->set_opacity(0);
                    return false;
              }, 400);
          }
      );      

      Gtk::Button * buttonPtrDone = nullptr;
      ui->get_widget<Gtk::Button>("csv_importButtonDone", buttonPtrDone);
      buttonPtrDone->signal_clicked().connect(      
      [=]()
          {
              Gtk::Window * mainWindowToDisable = nullptr;
              ui->get_widget<Gtk::Window>("mainWindow", mainWindowToDisable);
              mainWindowToDisable->set_sensitive(true);

              csvWindowDialog->hide();            
          }
      );

      ui->get_widget<Gtk::Button>("fileChosenCancel_csv", buttonPtr);
      buttonPtr->signal_clicked().connect(sigc::mem_fun(fileDialogWindow, &Gtk::ApplicationWindow::hide)); 

}

void DisplayWorkerGtk::DisplayImportStatus(musicDb::ImportedEntry parsedEntry, int currentImportPosition, int TotalImportCount)
{
    float currentProgress = (float)currentImportPosition / TotalImportCount;
    Gtk::ProgressBar * csvProgress = nullptr;
    ui->get_widget<Gtk::ProgressBar>("csv_importProgress", csvProgress);
    csvProgress->set_fraction(currentProgress);
    csvProgress->set_text(parsedEntry.artistName + " - " + parsedEntry.trackName + " /// " + std::to_string(currentProgress * 100) + "%");
};

void DisplayWorkerGtk::ImportDatabaseComplete(int timeToComplete)
{

    Gtk::Spinner * csvSpinner = nullptr;
    ui->get_widget<Gtk::Spinner>("csv_importProgressSpinner", csvSpinner);
    csvSpinner->set_opacity(0);
    
    Gtk::Button * buttonPtr_intrl = nullptr;
    ui->get_widget<Gtk::Button>("csv_importButtonDone", buttonPtr_intrl);
    buttonPtr_intrl->set_label("Done");
    buttonPtr_intrl->set_sensitive(true);     
}