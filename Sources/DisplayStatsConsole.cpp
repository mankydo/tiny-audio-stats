#include "DisplayStatsConsole.h"

void DisplayStatsConsole::DisplayTopAlbums(std::vector<std::pair<musicDb::album, int>> listOfAlbumsToDisplay)
{
    int ci = 1;
    printf("\n\nGot %i results\n", (int)listOfAlbumsToDisplay.size());
    printf("!Top 20 Albums all time!:\n\n");
    if(listOfAlbumsToDisplay.size() > 0)
    {
        printf("trying to displat\n");
        for(std::pair<musicDb::album, int> line: listOfAlbumsToDisplay)
        {
            printf("%i::: %i -- %s(id: %i) : '%s'(id: %i)\n", 
                ci, line.second, line.first.artistName.c_str(), line.first.artistId, line.first.name.c_str(), line.first.id);
            ci++;
        }
    }
    printf("Done!\n\n");
}

void DisplayStatsConsole::DisplayTopTracks(std::vector<std::pair<musicDb::track, int>> listOfTracksToDisplay)
{
    int ci = 1;
    printf("\ngot %i results\n", (int)listOfTracksToDisplay.size());
    printf("Top 20 Tracks all time:\n\n");
    for(std::pair<musicDb::track, int> line: listOfTracksToDisplay)
    {
        printf("%i::: %i -- %s : '%s' from '%s'\n", 
            ci, line.second, line.first.artistName.c_str(), line.first.name.c_str(), line.first.albumName.c_str());
        ci++;
    }
    printf("Done!\n\n"); 
}

void DisplayStatsConsole::DisplayTopArtists(std::vector<std::pair<musicDb::artist, int>> listOfArtistToDisplay)
{
    int ci = 1;
    printf("\n\ngot %i results\n", (int)listOfArtistToDisplay.size());
    printf("Top 20 Artists all time:\n\n");
    for(std::pair<musicDb::artist, int> line: listOfArtistToDisplay)
    {
        printf("%i::: %i -- %s\n", 
            ci, line.second, line.first.name.c_str());
        ci++;
    } 
    printf("Done!\n\n");
}

void DisplayStatsConsole::DisplayInfo(std::string info){
    printf("\nMessage is:\n %s\n\n", info.c_str());
}