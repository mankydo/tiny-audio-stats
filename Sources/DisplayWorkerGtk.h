#ifndef GTKMM_GTKFRONTENDFORSTATS_H
#define GTKMM_GTKFRONTENDFORSTATS_H

#include <gtkmm.h>
#include "IDisplayStats.h"
#include "PopupDatePickGtk.h"


class DisplayWorkerGtk: public Gtk::Application, public IDisplayStats
{
    protected:
      DisplayWorkerGtk();

    public:
      static Glib::RefPtr<DisplayWorkerGtk> create();
      static Glib::RefPtr<DisplayWorkerGtk> create(IAppController * appControllerToUse);

      DisplayWorkerGtk(IAppController * appControllerToUse): Gtk::Application("org.my.tinyaydiostats", Gio::APPLICATION_HANDLES_OPEN)
      {
          Init(appControllerToUse);
      };

      void Init(IAppController * appControllerToUse) override
      {
          _appController = appControllerToUse;
          _appController->AddDisplayWorker(this);
      };

      virtual void DisplayImportStatus(musicDb::ImportedEntry parsedEntry, int currentImportPosition, int TotalImportCount) override;
      virtual void ImportDatabaseComplete(int timeToComplete) override;
      virtual void DisplayTopAlbums(std::vector<std::pair<musicDb::album, int>> listOfAlbumsToDisplay) override;
      virtual void DisplayTopTracks(std::vector<std::pair<musicDb::track, int>> listOfTracksToDisplay) override;
      virtual void DisplayTopArtists(std::vector<std::pair<musicDb::artist, int>> listOfArtistToDisplay) override;
      virtual void DisplayInfo(std::string info) override;
      virtual void DisplayOverallStats(std::vector<std::string>) override {};

    protected:
      // Override default signal handlers:
      void on_activate() override;
      void on_open(const Gio::Application::type_vec_files& files,
        const Glib::ustring& hint) override;

      Gtk::Fixed * CreateEntry(int count, std::string line1, std::string line2, std::string line3, int parentWidth, float fraction_01);

      void clearBox(Gtk::Box * boxPointer, std::string prefix="fixed");

    private:
      std::string _applicationName = "Tiny Audio Stats v0.6.1";

      Glib::RefPtr<Gtk::Builder> ui;
      Gtk::ApplicationWindow * mainWindow;
      Gtk::Box * boxForTracks = nullptr;
      Gtk::Box * boxForArtists = nullptr;
      Gtk::Box * boxForAlbums = nullptr;
      Gtk::Label * infoLabel = nullptr;
      PopupDatePickGtk _popupCalendarWindow;

      int progressBarCount = 0;

      Gtk::ApplicationWindow* create_appwindow();
      void on_hide_window(Gtk::Window* window);

      void FileOperationsGUIBuilder();
};

#endif /* GTKMM_GTKFRONTENDFORSTATS_H */
