#ifndef DisplayStatsConsole_H
#define DisplayStatsConsole_H

#include "IAppController.h"
#include "IDisplayStats.h"

class DisplayStatsConsole: public IDisplayStats
{
public:

    DisplayStatsConsole(IAppController * appControllerToUse){
         Init(appControllerToUse);
    };

    void Init(IAppController * appControllerToUse) override
    {
        _appController = appControllerToUse;
        _appController->AddDisplayWorker(this);
    };

    virtual void DisplayImportStatus(musicDb::ImportedEntry parsedEntry, int currentImportPosition, int TotalImportCount) override {};
    virtual void ImportDatabaseComplete(int timeToComplete) override {};
    virtual void DisplayTopAlbums(std::vector<std::pair<musicDb::album, int>> listOfAlbumsToDisplay) override;
    virtual void DisplayTopTracks(std::vector<std::pair<musicDb::track, int>> listOfTracksToDisplay) override;
    virtual void DisplayTopArtists(std::vector<std::pair<musicDb::artist, int>> listOfArtistToDisplay) override;
    virtual void DisplayInfo(std::string info) override;
    virtual void DisplayOverallStats(std::vector<std::string>) override {};
};

#endif