#ifndef IAppController_H
#define IAppController_H

#include <vector>
#include "AudiostatsDatabase.h"
#include "MusicDbDatatypes.h"

class IDisplayStats;

enum TimeInterval
{
    week,
    weekBefore,
    month,
    year,
    custom
};

struct TimeIntervalToDisplay
{
    TimeInterval interval;
    musicDb::DayDate timeFrom = musicDb::DayDate(0,0,0);
    musicDb::DayDate timeTo = musicDb::DayDate(0,0,0);
};


class IAppController
{
protected:
    musicDb::AudiostatsDatabase * _audioDB;
    std::vector<IDisplayStats * > _displayWorkers;
    int _secondsPerDay = 86400;

    bool _b_ImportInProgress = false;
    int _b_EntriesToImportTotal = 0;
    int _b_EntriesProceed = 0;
    musicDb::ImportedEntry _b_CurrentEntryToImport;

public:
    virtual void Init(musicDb::AudiostatsDatabase * audioStatsDb) = 0;
    virtual void Init(std::string audioStatsDbFilename) = 0;

    virtual void AddDisplayWorker(IDisplayStats * workerToAdd) = 0;
    virtual void DeleteDisplayWorker(IDisplayStats * workerToDelete) = 0;

    virtual void ImportDatabase(string csvFilenameToImport, int timeToTruncNewEntries) = 0;
    virtual void ImportDatabaseBackground(string csvFilenameToImport, int timeToTruncNewEntries) = 0;
    virtual bool GetImportStatus(musicDb::ImportedEntry & parsedEntry, int & currentImportPosition, int & TotalImportCount) = 0;
    virtual void ImportDatabaseComplete(int timeToComplete) = 0;

    virtual void DisplayTopAlbums(time_t timeFrom, time_t timeTo) = 0;
    virtual void DisplayTopTracks(time_t timeFrom, time_t timeTo) = 0;
    virtual void DisplayTopArtists(time_t timeFrom, time_t timeTo) = 0;

    virtual void DisplayStatsAll() = 0;
    virtual void DisplayStats(TimeIntervalToDisplay intervalToUse) = 0;
    virtual void DisplayInfo(std::string info) = 0;

    virtual void DisplayOverallStats() = 0;

    virtual ~IAppController() {};
};

#endif