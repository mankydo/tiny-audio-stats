#ifndef audiostatsDatabase_H
#define audiostatsDatabase_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "../Sqlite3/sqlite3.h"
#include "BaseDatabase.h"
#include "MusicDbDatatypes.h"

using std::string;

namespace musicDb 
{
    class AudiostatsDatabase: public BaseDatabase{
        public:
            AudiostatsDatabase();
            AudiostatsDatabase(std::string dbFilename);
            ~AudiostatsDatabase();

            std::string GetDbFilename();

            virtual int ImportEntry(BaseEntry & entryToImport) override;
        

            //an idea for future use: 
            //  create three child objects: tracks, artists and albums
            //  and use those to communicate with the database
            //  something like that:
            //      int count = db.tracks.count(timePeriod)
            //      int count = db.artists.count(timePeriod)
            //  or
            //      vector<string> albums = db.artists.getAlbums(artistId)
            //  and so on, since most of the functions are pretty much the same 
            //  for each of the entities

            //add an artist and return it's ID
            int AddArtistToDb(string artistName);

            //add an album and return it's ID
            int AddAlbumToDb(int artistId, string albumName, int albumYear);
            //add an album and return it's ID
            int AddAlbumToDb(int artistId, string albumName);
            //add an album and return it's ID
            int AddAlbumToDb(string albumName);
            //add an artist to a specific album and return 0 if success
            int AddArtistToAlbum(int artistId, int albumId);

            //add a track and return it's ID
            int AddTrackToDb(string trackName, string ArtistName, string AlbumName);
            //add a track and return it's ID
            int AddTrackToDb(string trackName, int ArtistId, int AlbumId);
            //add a track and return it's ID
            int AddTrackToDb(string trackName, int ArtistId);

            int GetTrackId(string trackName, int artistId);
            int GetTrackId(string trackName, int artistId, int albumId);
            std::vector<int> GetTrackIds(string trackName);
            std::vector<int> GetTrackIds(string trackName, int artistId);
            std::vector<int> GetArtistTrackIds(int artistId);
            std::vector<int> GetAlbumTrackIds(int albumId);
//            std::vector<int> GetTrackId(string trackName, int albumId);

            std::vector<int> GetTracksId(string artistId);
//            std::vector<int> GetTracksId(string albumId);

            int GetAlbumId(string albumName);
            int GetAlbumId(string albumName, int artistId);
            std::vector<int> GetAlbumIds(string albumName);
            std::vector<int> GetAlbumIds(string albumName, int artistId);
            std::vector<int> GetAlbumIds(int artistId);

            int GetArtistId(string artistName);
            std::vector<int> GetArtistIds(string artistName);


            //Add a track to TrackListened and return an error status; 0 if success
            int AddScrobbledTrack(int trackId, time_t unixTime);

            std::vector<int, int> getCountOfTrack(int trackId);
            std::vector<int, int> getCountOfArtist(int trackId);
            std::vector<int, int> getCountOfAlbum(int trackId);

            //later on:
            //get count of track/artist/

            //Next methods are implemented in AudiostatsDatabaseQueries.cpp

            //Returns list of tracks sorted from top to bottom by count 
            std::vector<std::pair<track, int>> TopTracksInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned=20);
            //Returns list of tracks sorted from top to bottom by count
            std::vector<std::pair<track, int>> TopTracksInInterval(time_t timeTo)
                {return TopTracksInInterval(time_t(0), timeTo); };

            //Returns list of artists sorted from top to bottom by count
            std::vector<std::pair<musicDb::artist, int>> TopArtistsInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned=20);
            //Returns list of artists sorted from top to bottom by count
            std::vector<std::pair<musicDb::artist, int>> TopArtistsInInterval(time_t timeTo)
                {return TopArtistsInInterval(0, timeTo); };

            //Returns list of albums sorted from top to bottom by count
            std::vector<std::pair<musicDb::album, int>> TopAlbumsInInterval(time_t timeFrom, time_t timeTo, int numberOfEntriesReturned=20);
            std::vector<std::pair<musicDb::album, int>> TopAlbumsInInterval(time_t timeTo)
                { return TopAlbumsInInterval(0, timeTo); };

        protected:
            int InitDatabase(sqlite3 * dB) override;
    };
}

#endif