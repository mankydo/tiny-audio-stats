#ifndef MusicDbDatatypes_H
#define MusicDbDatatypes_H

#include <string>
#include "BaseDatabase.h"

namespace musicDb 
{
    struct ImportedEntry : public BaseEntry
    {
        string date;
        string time;
        time_t unixTime;
        string artistName;
        string albumName;
        string trackName;
//        virtual ~ImportedEntry(){};
    };

    struct musicObject
    {
        int id;
        string name;
        std::vector<string> tags;
    };

    struct track : musicObject
    {
        int artistId;
        string artistName;
        int albumId;
        string albumName;
        int durationInSeconds;
        int year;
    };

    struct artist : musicObject
    {
        string description;
    };

    struct album : musicObject
    {
        string description;
        int artistId;
        string artistName;
        int year;
    };

    class DayDate
    {
        private:
            
            int _day;
            int _month;
            int _year;

            time_t _dateStartTime;
            time_t _dateEndTime;

            int _secondsPerDay = 86400;

        public:

            DayDate(int day, int month, int year)
            {
                _day = day;
                _month = month;
                _year = year;

                SetTimes(day, month, year);
            };

            inline time_t GetDateStartTime(){ return _dateStartTime; };
            inline time_t GetDateEndTime(){ return _dateEndTime; };
            inline int GetDay(){ return _day; };
            inline int GetMonth(){ return _month; };
            inline int GetYear(){ return _year; };

            void SetTimes(int day, int month, int year)
            {   
                _day = day;
                _month = month;
                _year = year;

                struct tm * timeinfo;

                time_t tempTime = time(0);
                timeinfo = localtime(&tempTime);

                timeinfo->tm_year   = year - 1900;
                timeinfo->tm_mon    = month;    //months since January - [0,11]
                timeinfo->tm_mday   = day;          //day of the month - [1,31] 
                timeinfo->tm_hour   = 0;         //hours since midnight - [0,23]
                timeinfo->tm_min    = 0;          //minutes after the hour - [0,59]
                timeinfo->tm_sec    = 0;  

                _dateStartTime = mktime (timeinfo);

                timeinfo->tm_year   = year - 1900;
                timeinfo->tm_mon    = month;    //months since January - [0,11]
                timeinfo->tm_mday   = day;          //day of the month - [1,31] 
                timeinfo->tm_hour   = 23;         //hours since midnight - [0,23]
                timeinfo->tm_min    = 59;          //minutes after the hour - [0,59]
                timeinfo->tm_sec    = 59;  

                _dateEndTime = mktime(timeinfo);
            }

            void SetTimes(time_t timeToSet)
            {
                struct tm * timeinfo;
                timeinfo = localtime ( &timeToSet);
                _day = timeinfo->tm_mday;
                _month = timeinfo->tm_mon;
                _year = timeinfo->tm_year + 1900;

                SetTimes(_day, _month, _year);
            }

    };
}

#endif