#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "ImportDatabase.h"
#include "AudiostatsDatabase.h"
#include "AppController.h"
#include "DisplayStatsConsole.h"
#include "DisplayWorkerGtk.h"

using std::string;
  
int main(int argc, char* argv[])
{
    printf("Tiny audio database stats: \n");
    printf("Command line options are: \n");
    printf(" cli -- for displaying via command line\n");
    printf(" f filename -- for importing file to database\n");
    printf(" db dbfilename -- for choosing a database file name\n");
    printf(" ./program f 'filename.csv' db 'dbfilename'\n\n");

//    string filename = "test.csv";
//    string dBfilename = "test_statsdb.db";
    string filename = ""; 
    string dBfilename = "";
    string settingsFilename = "tinyAudioStats.ini";
    time_t timeToTruncNewEntries = 10;
    bool cliDisplay = false;

    std::fstream settingsFile(settingsFilename, std::ios::in);
    if(settingsFile.is_open()){
        std::string newLine;

        printf("opened a file!\n");

        while(std::getline(settingsFile, newLine)){

            //file reading here
            if(newLine != "")
            {
                int pos = newLine.find("db=");
                printf("pos is %i\n", pos);
                if(pos!=-1)
                    dBfilename = newLine.substr(3);
            }
        }
    } else 
    {
        settingsFile.open(settingsFilename, std::ios::out);
        printf("created a file!\n");
    }

    if(argc > 1)
    {
        std::string arg(argv[1]);
//        std::printf("First input argument is: %s\n", arg.c_str());

        for(int i = 0; i < argc; i++){
            if((string)argv[i] == "f"){
                filename = (string)argv[i+1];
                printf("Got a filename to import: %s\n\n", filename.c_str());
            }
            if((string)argv[i] == "db"){
                dBfilename = (string)argv[i+1];
                printf("Got a db to operate upon: %s\n\n", dBfilename.c_str());
            }
            if((string)argv[i] == "cli"){
                cliDisplay = true;
                printf("Command line output enabled. \n\n");
            }
        }
    }

    printf("Database to work with: %s\n", dBfilename.c_str());
    printf(".CSV filename to work with: %s\n\n", filename.c_str());


//    musicDb::AudiostatsDatabase audioDatabase(dBfilename);
//    AppController appController(&audioDatabase); 
    AppController appController(dBfilename); 
    DisplayStatsConsole displayStatsCli(&appController);

    //it should be saved in appController so it can be manipulated during runtime but for now that will do
    std::string settingsToWrite = "db=" + dBfilename + "\n";
    settingsFile << settingsToWrite;

    appController.ImportDatabase(filename, timeToTruncNewEntries);


    time_t currentTime = time(0);

//      test case:
//    std::vector<std::pair<musicDb::track, int>> topTracks = audioDatabase.TopTracksInInterval(1638319663, 1635727663);

//      test case:
//    std::vector<std::pair<musicDb::album, int>> topAlbums = audioDatabase.TopAlbumsInInterval(1638319663, 1635727663);

    if(cliDisplay){
        printf("Command line mode:\n\n");

        appController.DisplayTopTracks(0, currentTime);
        appController.DisplayTopAlbums(0, currentTime);
        appController.DisplayTopArtists(0, currentTime);

        appController.DisplayStats(TimeIntervalToDisplay{TimeInterval::week});

        return 0;
    }

    auto application = DisplayWorkerGtk::create(&appController);

    return application->run(argc, argv);
}
