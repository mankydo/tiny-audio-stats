#include <fstream>
#include "BaseDatabase.h"

BaseDatabase::BaseDatabase(string dBfilename)
{
        std::fstream dBFile;
        sqlite3 * db;
        bool noErrors = true;
        if(dBfilename != "")
        {

            printf("Opening database:\n");

            std::ifstream ifile(dBfilename);
            if(ifile)
            {
                // open database
                db = OpenDatabase(dBfilename);
                if(db == nullptr)
                    noErrors = false;
            } else 
            {
                printf("No file exists;\n");
                //create database

                db = OpenDatabase(dBfilename);
                if(db == nullptr) {
                    noErrors = false;
                }
            }
        } else {
            noErrors = false;
        }

    if(noErrors)
    {
        _dbFilename = dBfilename;
        _DB = db;
    } else {
        throw std::runtime_error("Error opening database; exiting!\n");
    };    

//    InitDatabase(db);
}

BaseDatabase::~BaseDatabase()
{
    sqlite3_close(_DB);
}

sqlite3 * BaseDatabase::OpenDatabase(string dBfilename)
{
    printf("Trying to open database: %s\n", dBfilename.c_str());
    sqlite3 * db;
    int exit = 0;
    exit = sqlite3_open(dBfilename.c_str(), &db);

    if (exit) {
        std::cerr << "Error open DB " << sqlite3_errmsg(db) << std::endl;
        db = nullptr;
    }
    else {
        std::cout << "Opened Database Successfully!" << std::endl;
    }

    return db;
}

int BaseDatabase::ExecuteSql(string statement)
{
    int errorCode = 0;
    char * errorMessage;

    errorCode = sqlite3_exec(_DB, statement.c_str(), NULL, 0, &errorMessage);

    if(errorCode != SQLITE_OK){
        printf("Sql execution failed, error is:: %i: %s\n ____sql: %s\n", errorCode, errorMessage, statement.c_str());
        sqlite3_free(errorMessage);
    } else 
    {
//        printf("Sql execution success!\n");
    }  

    return errorCode;
}

//returns all rows for a queue or a vector with size()=0 otherwise
std::vector<std::vector<string>> BaseDatabase::GetAllRowsOfSelect(string selectSql)
{
    std::vector<std::vector<string>> result;
    
    printf("executing select: %s\n", selectSql.c_str());

    int errorCode = 0;
    char * errorMessage;
    auto selectResponce = [](void* dObject, int columns, char** values, char** ColumnsName)
    {
        std::vector<string> result;

        for(int i=0; i < columns; i++)
        {
//            printf("adding: %s\n", values[i]);
            result.push_back((string)values[i]);
        }

        std::vector<std::vector<string>> * out = reinterpret_cast<std::vector<std::vector<string>> *>(dObject);
        out->push_back(result);

//        printf("select is: %s\n", result.at(0).c_str());
        return 0;
    };

    errorCode = sqlite3_exec(_DB, selectSql.c_str(), selectResponce,static_cast<void *>(&result), &errorMessage);

    if(errorCode != SQLITE_OK){
        printf("select all rows failed; error is %i, %s\n ___sql statement: %s", errorCode, errorMessage, selectSql.c_str());
        sqlite3_free(errorMessage);
    } else
        printf("select of all rows success!\n");

    return result;
}

//returns first row for a queue or a vector with size()=0 otherwise
std::vector<string> BaseDatabase::GetFirstRowOfSelect(string selectSql)
{
    std::vector<string> result;

    int errorCode = 0;
    char * errorMessage;
    sqlite3_stmt *sqlStatement;
    errorCode = sqlite3_prepare_v2(_DB, selectSql.c_str(), -1, &sqlStatement, NULL);

    if(errorCode != SQLITE_OK){
        string errmsg(sqlite3_errmsg(_DB));
        printf("Select prepare failed: %s\n", errmsg.c_str());
        printf("sql is: %s \n\n", selectSql.c_str());
        sqlite3_finalize(sqlStatement);
    }
        else 
    {
        errorCode = sqlite3_step(sqlStatement);
        if(errorCode != SQLITE_ROW)
        {
            string errmsg(sqlite3_errmsg(_DB));
            printf("error code: %i; Select failed: %s\n", errorCode, errmsg.c_str());
            printf("sql is: %s \n\n", selectSql.c_str());
            sqlite3_finalize(sqlStatement);
        } else {
            printf("Select success!\n");
            for(int i = 0; i < sqlite3_column_count(sqlStatement); i++ )
            {
                string tempValue;
                bool error = false;

                if(sqlite3_column_type(sqlStatement, i) == SQLITE_INTEGER) 
                {
                    tempValue =  std::to_string(sqlite3_column_int(sqlStatement, i));
                } 
                else if(sqlite3_column_type(sqlStatement, i) == SQLITE_FLOAT)
                {
                    tempValue =  std::to_string(sqlite3_column_double(sqlStatement, i));
                } else 
                {

                    if(sqlite3_column_text(sqlStatement, i) != NULL){
                        tempValue =  string(reinterpret_cast<const char *>(sqlite3_column_text(sqlStatement, i)));
                        
                    } else {
                        printf("nothing found;\n");
                        error = true;
                    }
                }
                 
                if(!error) 
                    result.push_back(tempValue);
            }

            sqlite3_finalize(sqlStatement);
        }
    }

/*     if(result.size() == 0){
        result.push_back(std::to_string(DbErrors::notfound));
    } */

    return result;
}

int BaseDatabase::getLastRowId()
{
    int rowId = stoi(GetFirstRowOfSelect("select last_insert_rowid();").at(0));
    return rowId;
}
int BaseDatabase::getLastRowIdFromTable(string tableName)
{
    printf("getting rowId\n");
    std::vector<string> resultRows =  GetFirstRowOfSelect("select max(rowid) from '" + tableName + "';");

    int rowId = resultRows.size() > 0 ? stoi(resultRows.at(0)) : 0;
    printf("rowid = %i\n", rowId);
    return rowId;
}

int BaseDatabase::StartTransaction()
{
    int errorCode = ExecuteSql("BEGIN TRANSACTION;");

    if(errorCode == SQLITE_OK) {
        TransactionInProgress = true;
    }
    
    return errorCode;
}
    
int BaseDatabase::CommitTransaction()
{
    int errorCode = 0;

    if(TransactionInProgress)
    {
        errorCode = ExecuteSql("COMMIT;");
        if(errorCode == SQLITE_OK)
        {
            TransactionInProgress = false;
        }
    }

    return errorCode;
}